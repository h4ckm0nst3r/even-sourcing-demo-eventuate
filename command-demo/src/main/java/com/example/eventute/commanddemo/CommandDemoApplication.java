package com.example.eventute.commanddemo;

import io.eventuate.javaclient.driver.EventuateDriverConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(EventuateDriverConfiguration.class)
public class CommandDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommandDemoApplication.class, args);
    }

}
