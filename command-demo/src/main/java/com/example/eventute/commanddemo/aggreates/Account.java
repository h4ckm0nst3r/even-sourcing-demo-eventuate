package com.example.eventute.commanddemo.aggreates;

import com.example.eventute.commanddemo.commands.AccountCommand;
import com.example.eventute.commanddemo.commands.AccountDebitFailedDueToInsufficientFundsEvent;
import com.example.eventute.commanddemo.commands.CreateAccountCommand;
import com.example.eventute.commanddemo.commands.CreditCommand;
import com.example.eventute.commanddemo.commands.DebitCommand;
import com.example.eventute.commanddemo.events.AccountCredited;
import com.example.eventute.commanddemo.events.AccountDebited;
import com.example.eventute.commanddemo.events.CreatedAccountEvent;
import io.eventuate.Event;
import io.eventuate.EventUtil;
import io.eventuate.ReflectiveMutableCommandProcessingAggregate;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Getter
public class Account extends ReflectiveMutableCommandProcessingAggregate<Account, AccountCommand> {

    private double balance;

    public List<Event> process(CreateAccountCommand cmd) {

        return EventUtil.events(new CreatedAccountEvent(cmd.getAmount()));
    }

    public List<Event> process(DebitCommand cmd) {
        if (balance < cmd.getAmount())
            return EventUtil.events(
                    new AccountDebitFailedDueToInsufficientFundsEvent(cmd.getId()));
        else
            return EventUtil.events(
                    new AccountDebited(cmd.getId(), cmd.getAmount()));
    }

    public List<Event> process(CreditCommand cmd) {
        return EventUtil.events(
                new AccountCredited(cmd.getId(), cmd.getAmount()));
    }

    public void apply(CreatedAccountEvent event) {
        balance = event.getAmount();
    }

    public void apply(AccountDebited event) {
        balance = balance - event.getAmount();
    }

    public void apply(AccountDebitFailedDueToInsufficientFundsEvent event) {
    }

    public void apply(AccountCredited event) {
        balance = balance + event.getAmount();
    }


}
