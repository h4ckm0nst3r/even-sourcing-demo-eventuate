package com.example.eventute.commanddemo.aggreates;

import com.example.eventute.commanddemo.events.AccountCredited;
import com.example.eventute.commanddemo.events.AccountDebited;
import io.eventuate.EventHandlerContext;
import io.eventuate.EventHandlerMethod;
import io.eventuate.EventSubscriber;

import java.util.concurrent.CompletableFuture;

@EventSubscriber(id="commandSideEventHandlers")
public class
AccountWorkflow {

//    @EventHandlerMethod
//    public CompletableFuture<?> debitAccount(EventHandlerContext<AccountDebited> ctx) {
//        AccountDebited event = ctx.getEvent();
//        double amount = event.getAmount();
//        String transactionId = ctx.getEntityId();
//
//        return ctx.update(Account.class, fromAccountId,
//                new DebitAccountCommand(amount, transactionId));
//    }
//
//    @EventHandlerMethod
//    public CompletableFuture<?> creditAccount(EventHandlerContext<DebitRecordedEvent> ctx) {
//    ...
//    }
}
