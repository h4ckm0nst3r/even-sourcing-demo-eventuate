package com.example.eventute.commanddemo.commands;

import io.eventuate.Event;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AccountDebitFailedDueToInsufficientFundsEvent implements Event {

    String id;
}
