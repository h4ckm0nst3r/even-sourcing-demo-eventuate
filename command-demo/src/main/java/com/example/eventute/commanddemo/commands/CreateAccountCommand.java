package com.example.eventute.commanddemo.commands;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateAccountCommand implements AccountCommand {
    private double amount;
}
