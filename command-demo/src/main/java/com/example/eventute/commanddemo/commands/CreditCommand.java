package com.example.eventute.commanddemo.commands;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CreditCommand implements AccountCommand {
    private String id;
    private double amount;
}
