package com.example.eventute.commanddemo.config;

import com.example.eventute.commanddemo.aggreates.Account;
import com.example.eventute.commanddemo.commands.AccountCommand;
import com.example.eventute.commanddemo.snapshot.AccountSnapshotStrategy;
import io.eventuate.EventuateAggregateStore;
import io.eventuate.javaclient.spring.EnableEventHandlers;
import io.eventuate.AggregateRepository;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
@EnableEventHandlers
public class Configuration {

    @Bean
    public AccountSnapshotStrategy accountSnapshotStrategy() {
        return new AccountSnapshotStrategy();
    }

    @Bean
    public AggregateRepository<Account, AccountCommand> acccountRepository(EventuateAggregateStore eventStore){
        return new AggregateRepository<>(Account.class, eventStore);
    }
}
