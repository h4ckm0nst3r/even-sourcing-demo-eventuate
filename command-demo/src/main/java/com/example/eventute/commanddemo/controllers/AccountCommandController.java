package com.example.eventute.commanddemo.controllers;

import com.example.eventute.commanddemo.aggreates.Account;
import com.example.eventute.commanddemo.controllers.dto.CreateAccountRequest;
import com.example.eventute.commanddemo.services.AccountService;
import io.eventuate.EntityWithIdAndVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;

@RestController
public class AccountCommandController {
    Logger logger= LoggerFactory.getLogger(AccountCommandController.class);

    @Autowired
    AccountService service;



    @PostMapping(value = "/accounts")
    public CompletableFuture<EntityWithIdAndVersion<Account>> createAccount(CreateAccountRequest request){
        logger.info("Receive request: ", request.toString());
        return service.openAccount(request.getBalance());
    }
}
