package com.example.eventute.commanddemo.controllers.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class CreateAccountRequest {
    double balance;
}
