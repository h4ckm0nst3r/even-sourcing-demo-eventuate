package com.example.eventute.commanddemo.events;


import io.eventuate.Event;
import io.eventuate.EventEntity;

@EventEntity(entity="com.example.eventute.commanddemo.aggreates.Account")
interface AccountEvent extends Event
{
}
