package com.example.eventute.commanddemo.services;

import com.example.eventute.commanddemo.aggreates.Account;
import com.example.eventute.commanddemo.commands.AccountCommand;
import com.example.eventute.commanddemo.commands.CreateAccountCommand;
import com.example.eventute.commanddemo.commands.CreditCommand;
import com.example.eventute.commanddemo.commands.DebitCommand;
import io.eventuate.EntityWithIdAndVersion;
import io.eventuate.AggregateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


import java.util.concurrent.CompletableFuture;

@Service
public class AccountService {
    Logger logger= LoggerFactory.getLogger(AccountService.class);
    private final AggregateRepository<Account, AccountCommand> acccountRepository;

    public AccountService(AggregateRepository<Account, AccountCommand> acccountRepository) {
        this.acccountRepository = acccountRepository;
    }

    public CompletableFuture<EntityWithIdAndVersion<Account>> openAccount(double initialBalance) {
        logger.info("open account service start ");
        return acccountRepository.save(new CreateAccountCommand(initialBalance));
    }

    public CompletableFuture<EntityWithIdAndVersion<Account>> credit(String id, double initialBalance) {
        return acccountRepository.update(id, new CreditCommand(id, initialBalance));
    }

    public CompletableFuture<EntityWithIdAndVersion<Account>> debit(String id, double initialBalance) {
        return acccountRepository.update(id, new DebitCommand(id, initialBalance));
    }

}
