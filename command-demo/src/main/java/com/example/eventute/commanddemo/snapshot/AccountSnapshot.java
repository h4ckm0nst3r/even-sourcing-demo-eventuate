package com.example.eventute.commanddemo.snapshot;

import io.eventuate.Snapshot;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AccountSnapshot implements Snapshot {

    private double balance;
}
