package com.example.eventute.commanddemo.snapshot;

import com.example.eventute.commanddemo.aggreates.Account;
import com.example.eventute.commanddemo.commands.CreateAccountCommand;
import io.eventuate.*;

import java.util.List;
import java.util.Optional;

public class AccountSnapshotStrategy implements SnapshotStrategy {
    @Override
    public Class<?> getAggregateClass() {
        return Account.class;
    }

    @Override
    public Optional<Snapshot> possiblySnapshot(Aggregate aggregate, Optional<Int128> snapshotVersion, List<EventWithMetadata> oldEvents, List<Event> newEvents) {
        if(oldEvents.size()<10){
            return Optional.empty();
        }
        Account account = (Account) aggregate;
        return Optional.of(new AccountSnapshot(account.getBalance()));
    }

    @Override
    public Aggregate recreateAggregate(Class<?> aClass, Snapshot snapshot, MissingApplyEventMethodStrategy missingApplyEventMethodStrategy) {
        AccountSnapshot accountSnapshot = (AccountSnapshot) snapshot;
        Account aggregate = new Account();
        List<Event> events = aggregate.process(new CreateAccountCommand(accountSnapshot.getBalance()));
        Aggregates.applyEventsToMutableAggregate(aggregate, events, missingApplyEventMethodStrategy);
        return aggregate;
    }
}
