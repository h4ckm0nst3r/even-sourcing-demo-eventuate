package com.example.eventute.querydemo.controller;

import com.example.eventute.querydemo.services.AccountQueryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

    AccountQueryService service;

    public AccountController(AccountQueryService service) {
        this.service = service;
    }

    @GetMapping(value = "/{accountId}")
    public ResponseEntity<?> getAccount(@PathVariable("account-id") String accountId){
        return ResponseEntity.ok(service.getAccount(accountId)) ;
    }

    @GetMapping(value = "/")
    public ResponseEntity<?> getAccount(){
        return ResponseEntity.ok(service.getAllAccounts()) ;
    }
}
