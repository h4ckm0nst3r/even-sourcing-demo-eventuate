package com.example.eventute.querydemo.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AccountCredited implements AccountEvent {
    String id;
    double amount;
}
