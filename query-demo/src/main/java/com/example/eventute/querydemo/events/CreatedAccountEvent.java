package com.example.eventute.querydemo.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CreatedAccountEvent implements AccountEvent {
    double amount;
}
