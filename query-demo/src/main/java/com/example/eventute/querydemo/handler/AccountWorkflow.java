package com.example.eventute.querydemo.handler;

import com.example.eventute.querydemo.events.CreatedAccountEvent;
import com.example.eventute.querydemo.services.AccountInfoUpdateService;
import io.eventuate.DispatchedEvent;
import io.eventuate.EventHandlerMethod;
import io.eventuate.EventSubscriber;
import org.springframework.stereotype.Service;

@Service
@EventSubscriber(id="querySideEventHandlers")
public class AccountWorkflow {

    private AccountInfoUpdateService accountInfoUpdateService;

    public AccountWorkflow(AccountInfoUpdateService accountInfoUpdateService) {
        this.accountInfoUpdateService = accountInfoUpdateService;
    }

    @EventHandlerMethod
    public void create(DispatchedEvent<CreatedAccountEvent> de) {
        CreatedAccountEvent event = de.getEvent();
        String id = de.getEntityId();
//        String eventId = de.getEventId().asString();
        double initialBalance = event.getAmount();
        accountInfoUpdateService.create(id, initialBalance);
    }
}