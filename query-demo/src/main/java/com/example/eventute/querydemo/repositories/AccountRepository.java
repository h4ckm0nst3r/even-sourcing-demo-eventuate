package com.example.eventute.querydemo.repositories;

import com.example.eventute.querydemo.entity.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, String> {
}
