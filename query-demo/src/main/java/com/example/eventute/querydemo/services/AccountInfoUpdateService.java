package com.example.eventute.querydemo.services;

import com.example.eventute.querydemo.entity.Account;
import com.example.eventute.querydemo.repositories.AccountRepository;
import org.springframework.stereotype.Service;

@Service
public class AccountInfoUpdateService {

    AccountRepository accountRepository;

    public AccountInfoUpdateService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void create(String id, double initialBalance) {
        accountRepository.save(new Account(id, initialBalance));
    }
}
