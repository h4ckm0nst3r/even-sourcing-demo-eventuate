package com.example.eventute.querydemo.services;

import com.example.eventute.querydemo.entity.Account;
import com.example.eventute.querydemo.repositories.AccountRepository;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountQueryService {

    AccountRepository repository;

    public AccountQueryService(AccountRepository repository){
        this.repository= repository;
    }


    public Account getAccount(String id){
        return this.repository.findById(id).orElse(null);
    }

    public List<Account> getAllAccounts() {
        return IterableUtils.toList(repository.findAll());
    }
}
